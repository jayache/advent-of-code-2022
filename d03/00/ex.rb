input = File.read("input").chomp
p input.split("\n").map {|l| l.split(",").map {|p| (p.split("-")[0].to_i..p.split("-")[1].to_i)}}.map {|l| l[0].to_a.all? {|n| l[1].include? n} || l[1].to_a.all? {|n| l[0].include? n}}.count(true)
