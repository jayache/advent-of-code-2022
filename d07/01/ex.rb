input = File.read("input").split("\n")
$transposed = input.map {|l| l.split("")}.transpose
def scenic(map, x, y)
  a = 0
  map[y][0...x].reverse.each_char {|t| a += 1; break if t.to_i >= map[y][x].to_i}
  b = 0
  map[y][(x+1)..-1].each_char {|t| b += 1; break if t.to_i >= map[y][x].to_i}
  c = 0
  $transposed[x][0...y].reverse.each {|t| c += 1; break if t.to_i >= map[y][x].to_i}
  d = 0
  $transposed[x][(y+1)..-1].each {|t| d += 1; break if t.to_i >= map[y][x].to_i}
  a * b * c * d
end
max = 0
input.each.with_index do |l, y|
  l.each_char.with_index do |t, x|
    max = [max, scenic(input, x, y)].max
  end
end
p max
