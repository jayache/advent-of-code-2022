input = File.read("input").split("\n")
counter = 0
input.each.with_index do |l, y|
  l.each_char.with_index do |t, x|
    transpose = input.map {|l| l.split("")}.transpose.map {|l| l.join}
    counter += 1  if input[y][0...x].each_char.select {|c| c.to_i >= t.to_i}.size == 0 or
      input[y][(x+1)..-1].each_char.select {|c| c.to_i >= t.to_i}.size == 0 or
      transpose[x][0...y].each_char.select {|c| c.to_i >= t.to_i}.size == 0 or
      transpose[x][(y+1)..-1].each_char.select {|c| c.to_i >= t.to_i}.size == 0
  end
end
p counter
