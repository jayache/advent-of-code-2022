require 'matrix'

input = File.read("input").split("\n")
rope = Array.new(2).map {Vector[0, 0]}
positions = Hash.new()
dir = {"U" => Vector[0, 1], "D" => Vector[0, -1], "R" => Vector[1, 0], "L" => Vector[-1, 0]}
input.each {|d| d.split[1].to_i.times {rope[0] += dir[d.split[0]]; rope[1..-1].size.times {|i| rope[i+1] += (rope[i] - rope[i+1]).normalize.map {|n| if n > 0 then n.ceil else n.floor end} if (rope[i] - rope[i+1]).magnitude >= 2}; positions[rope.last] = true }}
p positions.keys.size
