input = File.read("input").split "Monkey "

input = input[1..-1].map {
  |m| m.scan(/(\d+):\W+Starting items: ((\d+(, )?)*)\W+Operation: new = old (.*)\W+Test: divisible by (\d+)\W+If true: throw to monkey (\d+)\n\W+ If false: throw to monkey (\d+)/)}.map! {
    |m| m = m[0]; [m[1].split(", ").map(&:to_i), m[4], m[5].to_i, m[6].to_i, m[7].to_i] }
  mb = Array.new(input.size).map{0}
  20.times  { input.size.times {|i| m = input[i]; m[0].each {|t| t = eval("old = #{t}; #{t}#{m[1]}") / 3;if t % m[2] == 0 then input[m[3]][0].push t else input[m[4]][0].push t end; mb[i] += 1};  m[0].clear}}
  p mb.max(2).reduce(:*)
