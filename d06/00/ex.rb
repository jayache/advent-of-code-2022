input = File.read("input")
cd = ""
files = Hash.new()
input.split("\n").each do |l|
  if l == '$ cd /'
    cd = ''
  elsif l == "$ cd .."
    cd = cd.split("/")[0...-1].join("/")
  elsif l.start_with? "$ cd "
    cd = cd + "/" + l.scan(/(\$ cd )(\w+)/)[0][1]
  elsif l.start_with? "dir "
    files[cd] = [] if files[cd] == nil
    files[cd].append(cd + "/" + l.scan(/(dir )(\w+)/)[0][1])
  else
    files[cd] = [] if files[cd] == nil
    files[cd].append(l.scan(/\d+/).first.to_i)
  end
end

def size(files, k)
  files[k].map! {|f| if f.is_a? Integer then f else size(files, f) end}
  files[k].sum
end

size(files, "")
p files.each_key.to_a.map {|k| files[k].sum}.select {|d| d <= 100_000}.sum
