input = File.read("input").split "\n"
cycle = 1
$x = 1
screen = Array.new(6).map {"." * 40}
def exec(inst)
  op = inst.slice!(0)
  if op.start_with? /-?\d+/
    $x += op.to_i
  elsif op.start_with? 'addx '
    inst.prepend(op.split.last)
  end
end
loop { exec(input); screen[(cycle) / 40][(cycle) % 40] = '#' if ($x-1..$x+1).to_a.include?((cycle) % 40); cycle += 1; break if input.empty? }
puts screen
