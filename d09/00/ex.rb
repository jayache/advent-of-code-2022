input = File.read("input").split "\n"
cycle = 1
$x = 1
def exec(inst)
  op = inst.slice!(0)
  if op.start_with? /-?\d+/
    $x += op.to_i
  elsif op.start_with? 'addx '
    inst.prepend(op.split.last)
  end
end
19.times { exec(input); cycle += 1 }
sum = $x * cycle
40.times { exec(input); cycle += 1 }
sum += ($x * cycle)
40.times { exec(input); cycle += 1 }
sum += $x * cycle
40.times { exec(input); cycle += 1 }
sum += $x * cycle
40.times { exec(input); cycle += 1 }
sum += $x * cycle
40.times { exec(input); cycle += 1 }
sum += $x * cycle
p sum
