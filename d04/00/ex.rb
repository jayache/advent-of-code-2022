input = File.read("input").chomp
nb = (input.index("\n", input.index("1")) - input.index("1") + 2) / 4
stacks = Array.new(nb).map {|i| []}
input[0..input.index("1")].split("\n").map {|l| l.each_char.each_slice(4).to_a.map.with_index {|b, i| stacks[i % nb].unshift(b[1]) if b[0] == '['}}
input[input.index("move")..-1].split("\n").each {|l| o=l.scan(/(\d+).*(\d+).*(\d+)/)[0]; o[0].to_i.times {stacks[o[2].to_i-1].append stacks[o[1].to_i-1].pop}}
stacks.each {|e| print e.last}
puts
