input = File.read("input")
p input.split("\n").map {|l| l[0..(l.size/2 - 1)].each_char.to_a.select {|c| l[(l.size/2)..-1].include? c }.first}.map{|item| item.ord - (item.upcase == item ? 'A'.ord - 27 : 'a'.ord - 1)}.sum
