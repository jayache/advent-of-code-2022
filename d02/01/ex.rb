input = File.read("input")
p input.split("\n").each_slice(3).to_a.compact.map {|a, b, c| a.each_char.to_a.select {|k| b.include?(k) && c.include?(k)}.first}.map{|item| item.ord - (item.upcase == item ? 'A'.ord - 27 : 'a'.ord - 1)}.sum
