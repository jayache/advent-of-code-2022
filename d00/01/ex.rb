input = File.read('input')
p input.split("\n\n").collect {|l| l.split("\n").map(&:to_i).sum}.sort.reverse[0..2].sum
