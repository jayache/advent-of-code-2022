def eval_score(array)
  return 0 if array.empty? or array.nil?
  a = array.first
  b = array.last
  return b.ord - 'X'.ord + 1 +
    if a == (b.ord - 'X'.ord  + 'A'.ord).chr
      3
    elsif {'A' => 'Y', 'B' => 'Z', 'C' => 'X'}[a] == b
      6
    else
      0
    end
end

def find_result(move, goal)
  victory = {'A' => 'B', 'B' => 'C', 'C' => 'A'}
  if goal == 'Y'
    return move
  elsif goal == 'X'
    return victory.invert[move]
  else
    return victory[move]
  end
end

input = File.read('input').chomp
p input.each_line(chomp: true).collect {|s| eval_score([s.split.first, (find_result(*s.split).ord - 'A'.ord + 'X'.ord).chr])}.sum 
