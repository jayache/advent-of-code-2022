def eval_score(array)
  return 0 if array.empty? or array.nil?
  a = array.first
  b = array.last
  return b.ord - 'X'.ord + 1 +
    if a == (b.ord - 'X'.ord  + 'A'.ord).chr
      3
    elsif {'A' => 'Y', 'B' => 'Z', 'C' => 'X'}[a] == b
      6
    else
      0
    end
end

input = File.read('input')
p input.each_line(chomp: true).collect {|s| eval_score(s.split)}.sum 
